FROM alpine:3.18

RUN apk add --update --no-cache wget git curl bash unzip yq
RUN wget https://hashicorp-releases.yandexcloud.net/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip &&\
    unzip terraform_1.7.4_linux_amd64.zip &&\
    mv terraform /usr/bin/terraform
RUN wget https://get.helm.sh/helm-v3.14.2-linux-amd64.tar.gz -O - | tar -xz &&\
    mv linux-amd64/helm /usr/bin/helm &&\
    chmod +x /usr/bin/helm
RUN curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash &&\
    rm -rf terraform_1.7.4_linux_amd64.zip linux-amd64 install.sh /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV PATH="${PATH}:/root/yandex-cloud/bin"

COPY .terraformrc /root/.terraformrc

CMD ["tail", "-f", "/dev/null"]